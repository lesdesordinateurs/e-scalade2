# AndroidAppE-scalade
E-scalade is the code of an Android app designed to help blind people practice 
climbing.

This is done with a RFID system, with RFID tags placed on the climbing wall and
RFID readers at the climber's wrists worn during climing.
So far, 2 reader's brand are implemented into the app : 
- [the ACS NFC reader, model AC1255-UJ1](https://www.acs.com.hk/en/products/403/acr1255u-j1-acs-secure-bluetooth%C2%AE-nfc-reader/)
- [the SLS UHF reader, model SmartLINK](https://www.slsrfid.com/product/sls-smartlink-mobile-rfid-reader/)

When the climber catches a climbing hold, the reader at his wrist detect the tag's ID
fixed on the hold. This ID is then sent to the phone, which looks into a [CSV](https://framagit.org/lesdesordinateurs/e-scalade2/blob/master/Documentation/TypicalCSV.csv) file
modelling the wall wich hold is corresponding to the ID and which hold is next.
Then, it displays an audio indication using the phone's vocal synthesis 
reading to the climber the position and direction of the next hold on the wall.

The communication between the readers and the phone is done using **Bluetooth Low Energy**.

## **Prerequisites to use the app**

- Having a smartphone running on Android 4.4 at least (to support BLE)
- Having a vocal synthesis activated on your phone and up-to date

## How to use the app on your phone 
**So far, e-scalade is still available only in debug mode and not in release mode,**
which means that you can only run it with AndroidStudio and not as an APK file.
This should be soon possible, we just need to generate the necessary keys to release the app.

## **Documentation** 

In the [Documentation](https://framagit.org/lesdesordinateurs/e-scalade2/tree/master/Documentation) folder, you can find information about :
- how to use the app
- how to easily add new languages 
- how to (almost) easily add new types or new brands of readers
- some JavaDoc of the created classes

## Additional information and warning about the UHF readers (SmartLINK)

When received, the sample code to communicate with these readers was totally **opaque**.
Most of methods looked like the m32a method in the [UHFUtils](https://framagit.org/lesdesordinateurs/e-scalade2/blob/master/app/src/main/java/com/example/judith/bluetoothtest/Classes/UHF_SLS/UHFUtils.java) class : 
```java
static short m32a(byte[] byteArray,int i2) {
    int i3 = 0;
        short s = (short) -1;
        while (i3 < i2) {
            short s2 = (short) (s ^ (byteArray[1 + i3] << 8));
            for (int i4 = 0; i4 < 8; i4++) {
                if((32768 & s2) != 0)
                    s2 = (short)((s2 << 1)^ 4129);
                else
                    s2 = (short)(s2 <<1);
            }
            i3++;
            s = s2;
        }
        return s;
    }
```
Some work has been done to make the classes and methods more human-friendly, 
hower some are still not "decoded" so it might be a little confusing. You should 
be aware of that point when reading some of these classes like [FrameProcesing](https://framagit.org/lesdesordinateurs/e-scalade2/blob/master/app/src/main/java/com/example/judith/bluetoothtest/Classes/UHF_SLS/FrameProcessing.java). 
**The app is working though,** even with these bits of unreadable code. 

## Contributors
- Judith SALIC