package com.example.judith.bluetoothtest;

import androidx.test.espresso.accessibility.AccessibilityChecks;
import androidx.test.espresso.intent.rule.IntentsTestRule;

import com.example.judith.bluetoothtest.Activities.BluetoothActivity;
import com.example.judith.bluetoothtest.Activities.ValidationAudioActivity;

import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.core.IsNull.notNullValue;

public class ValidationActivityTest {

    @BeforeClass
    public static void enableAccessibilityChecks(){
        AccessibilityChecks.enable();
    }

    @Rule
    public IntentsTestRule<ValidationAudioActivity> mActivityRule = new IntentsTestRule<>(ValidationAudioActivity.class);

    @Test
    public void buttonScanTest(){

        onView(withId(R.id.btn_oui_validationAudio_activity)).check(matches(notNullValue()));
        onView(withId(R.id.btn_oui_validationAudio_activity)).perform(click());
    }

    @Test
    public void buttonClimbTest(){

        onView(withId(R.id.btn_listen_again_validationAudio_activity)).check(matches(notNullValue()));
        onView(withId(R.id.btn_listen_again_validationAudio_activity)).perform(click());
    }

    @Test
    public void buttonRecordTest(){

        onView(withId(R.id.btn_non_validationAudio_activity)).check(matches(notNullValue()));
        onView(withId(R.id.btn_non_validationAudio_activity)).perform(click());
    }
}
