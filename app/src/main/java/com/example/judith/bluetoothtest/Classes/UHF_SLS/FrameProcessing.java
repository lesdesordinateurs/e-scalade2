package com.example.judith.bluetoothtest.Classes.UHF_SLS;

import android.util.Log;

import java.util.Arrays;

/**
 * <b> Frame Processing is a class containing methods to process the frames received by the
 * readers.</b>
 * <p>
 *     The four methods described here go by pair : methods for an answer containing a tag's ID are
 *     checkTagIDFrameFormat and extractDataFromIDFrame.
 *     Methods to process any other frame coming from the reader are checkOtherFrameFormat and
 *     extractDataFromOtherFrame
 * </p>
 *
 */
class FrameProcessing {

    private static final int errorInt = -4;

    private byte[] mBytes = new byte[20];

    /**
     * Index kept to copy each frame read one after another into mBytes.For example, if a byteArray
     * of 40 bytes is copied to mBytes, the next byteArray to come will be copied in mBytes from the
     * index 40 so the first byteArray copied is not erased. */
    private int mIndexToCopy;

    /**
     * int to store the lenght of data bytes in the frame. The frame contains data bytes and
     * metadata bytes. The third and fourth bytes of the frame indicates the lenght of data in
     * number of bytes. This lenght is set to -1 when the object FrameProcessing is initialized and
     * then keeps the supposes data lenght.
     * */
    private int mDataLenght = -1;

    private boolean f38k;

    private boolean mTagRead = false;

    private static String TAG = "FrameProcessing";

    FrameProcessing(boolean tagRead) {
        this.mTagRead = tagRead;
        if (tagRead) { //if the expected answer is a tag's ID
            this.mBytes = new byte[0];
        }
    }
    /**
     * Stores each frame byteArray received one after the other into mBytes. Then when the last
     * frame is arrived, i.e. when this method is called for the last time, checks the content of
     * the frame.
     * @return 0 if the frame is valid and if it's the last frame, 1 if other frames are expected,
     * and another int if an error is found in the frame, i.e. the frame is not valid.
     * */
    int checkOtherFrameFormat(byte[] byteArray) {
        while (mBytes.length - mIndexToCopy < byteArray.length) {
            /* Expands  mBytes long enough to contain a whole number of frames of 20 bytes from byteArray.
             * For example, if the lenght of byteArray is 56 bytes, the final lenght of mBytes will
              * be of its original lenght + 40 byes (= 2 * 20 bytes) = 60 bytes in total.*/
            byte[] obj = new byte[(mBytes.length + 20)];
            System.arraycopy(mBytes, 0, obj, 0, mBytes.length); //copies entirely mBytes to obj
            mBytes = obj;

        }
        /* Copies the byteArray to mBytes now that mBytes has been enough expanded */
        System.arraycopy(byteArray, 0, mBytes, mIndexToCopy, byteArray.length);

        mIndexToCopy += byteArray.length;
        /* If this is the first frame (i.e. mDataLenght = -1, which means it hasn't been initialized yet
         * and if the byteArray contains data bytes (i.e. if its lenght is >= 5), sets mDataLenght
         * to the announced real lenght contained in the third and fourth metadata bytes. */
        if (mDataLenght == -1 && mIndexToCopy >= 5) {
            mDataLenght = ((mBytes[3] & 255) << 8) + (mBytes[4] & 255);
        }
        /* If this frame is not the last of the message, return 1 */
        if (mIndexToCopy < mDataLenght + 8) {
            return 1;
        }
        /* Each data frame needs to start with -69 and end with 126 (metadata bytes included) to be
         * valid.*/
        if (mBytes[0] != (byte) -69) {
            return -1;
        }
        if (mBytes[mDataLenght + 5] != (byte) 126) {
            return -2;
        }
        short a = UHFUtils.m32a(mBytes, mDataLenght + 5);

        /* A short has a lenght of two bytes. The short a needs to be equal to the concatenation
         * of both last bytes of mBytes. Otherwise, its an error. */
        if(mBytes[mDataLenght + 6] != ((byte) (a >>8)) ||(mBytes[mDataLenght + 7] != (byte)(a)))
            return -3;
        f38k = true;
        return 0;
    }

    /**
     * @return true if the frame to process is a tag ID, false if not
     */
    boolean isFrameTagID() {
        return this.mTagRead;
    }

    /**
     * Retrieves one frame after another an stores them in order into the mBytes byteArray. Then
     * checks the format of the frame (e.g, its lenght and metadatabytes) to be sure it's valid and
     * completed.
     * @param value
     *          the actual frame received to be treated.
     * @return an int indicating if the total frame's format is valid.
     */
    int checkTagIDFrameFormat(byte[] value) {
        Log.d(TAG, "checkTagIDFrameFormat - Frame received, mBytes lenght = " +mBytes.length);
        byte[] obj = new byte[(mBytes.length + value.length)];
        /* Copies the content of mBytes to a buffer obj */
        System.arraycopy(mBytes, 0, obj, 0, mBytes.length);
        /* Add to obj the content of value after mBytes, so obj has mBytes + value */
        System.arraycopy(value, 0, obj, mBytes.length, value.length);
        /* Updates mBytes to store the content of obj */
        mBytes = obj;
        Log.d(TAG, "checkTagIDFrameFormat - mBytes new lenght = " +mBytes.length);
        /* Data in each frame starts at byte 5, so if the length is shorter than 5, there isn't any data in the frame received.*/
        if (mBytes.length < 5) {
            return 1;
        }
        /* Third and fourth byte of the answer contains the length of the data in the frame. Typical is 18 for a tag ID */
        int dataLenght = ((this.mBytes[3] & 255) << 8) + (this.mBytes[4] & 255);
        Log.d(TAG, "checkTagIDFrameFormat -  data lenght = " + dataLenght);
        /* i2 = data lenght + lenght of one byte */
        int i2 = dataLenght + 8;
        Log.d(TAG, "checkTagIDFrameFormat -  I2 = " + i2);
        /* If the lenght of mBytes is inferior to the supposed lenght of the data + 8, then it
        means that some frames are following, so the FrameProcessing object needs to store the data
         and wait for the entire message to be received, i.e. to keep receiving the following frames.*/
        if (this.mBytes.length < i2) {
            return 1;
        }
        byte[] obj2 = new byte[i2];
        System.arraycopy(this.mBytes, 0, obj2, 0, i2);

        /* Each data frame needs to start with -69 and end with 126 (metadata bytes included) to be
        * valid.*/
        if (obj2[0] != (byte) -69) {
            return -1;
        }
        int i3 = dataLenght + 5;
        if (obj2[i3] != (byte) 126) {
            return -2;
        }
        short a = UHFUtils.m32a(obj2,i3);
        if (obj2[dataLenght + 6] == ((byte) (a >> 8))) {
            if (obj2[dataLenght + 7] == ((byte) a)) {
                if (mBytes.length > i2 && this.mBytes[2] == UHFUtils.AREA_US2) {
                    return 2;
                }
                if (this.mBytes.length == i2) {
                    if (this.mBytes[2] == UHFUtils.AREA_US2) {
                        return this.mBytes[1] == (byte) 3 ? 0 : 2;
                    } else {
                        if (this.mBytes[2] == (byte) -1) {
                            return 0;
                        }
                    }
                }
                return errorInt;
            }
        }
        return -3;
    }

    /**
     * Extract data from the byteArray mBytes and stores it into a FrameData object.
     * The byteArray of the FrameData object will contain only the data bytes. The two bytes of this
     * object will contain respectively the first and second byte of the frame (which has been
     * previously stored entirely in mBytes).
     * @return a new FrameData object containing extracted data from the frame(s) received.
     */
    FrameData extractDataFromOtherFrame() {
        if (!this.f38k) {
            return null;
        }
        byte[] obj = new byte[mDataLenght];
        /* Copies the entire content of mBytes without the 5 first bytes which are not data bytes.*/
        System.arraycopy(this.mBytes, 5, obj, 0, mDataLenght);
        return new FrameData(this.mBytes[1], this.mBytes[2], obj);
    }

    /**
     * Extract data from the byteArray mBytes and stores it into a FrameData object.
     * The byteArray of the FrameData object will contain only the data bytes. The two bytes of this
     * object will contain respectively the first and second byte of the frame (which has been
     * previously stored entirely in mBytes).
     * @return a new FrameData object containing extracted data from the frame(s) received.
     */
    FrameData extractDataFromIDFrame() {
        byte[] totalFrame = new byte[mBytes.length];
        /* get the lenght of data present in the frame. Indicated by the third and fourth bytes,
         * which are metadata bytes. */
        int dataLenght = ((this.mBytes[3] & 255) << 8) + (this.mBytes[4] & 255);

        Log.d(TAG, "extractDataFromIDFrame - data lenght = " +dataLenght);

        byte[] dataFrame = new byte[dataLenght];
        /* Copies the entire content of mBytes into dataFame without the 5 first bytes and the last
         3 bytes that are not data bytes but metadata bytes. */
        System.arraycopy(this.mBytes, 5, dataFrame, 0, dataLenght);
        /* Copies the entire content of mBytes with the metadata bytes into totalFrame.  */
        System.arraycopy(this.mBytes, 0, totalFrame, 0, totalFrame.length);

        Log.d(TAG, "extractDataFromIDFrame - dataFrame= " + Arrays.toString(dataFrame));
        Log.d(TAG, "extractDataFromIDFrame - totalFrame = " + Arrays.toString(totalFrame));

        byte[] remainingBytesFrame = new byte[((this.mBytes.length - dataLenght) - 8)];
        /* Store the remaining bytes in mBytes. */
        System.arraycopy(this.mBytes, dataLenght + 8, remainingBytesFrame, 0, remainingBytesFrame.length);
        this.mBytes = remainingBytesFrame;
        Log.d(TAG, "extractDataFromIDFrame - MBYTES (aka remainingBytesFrame) = " + Arrays.toString(mBytes));
        return new FrameData(totalFrame[1], totalFrame[2], dataFrame);
    }
}
