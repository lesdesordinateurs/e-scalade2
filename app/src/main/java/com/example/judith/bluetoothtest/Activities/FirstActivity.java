package com.example.judith.bluetoothtest.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.accessibility.AccessibilityManager;
import android.widget.Button;

import com.example.judith.bluetoothtest.Classes.DataKeeper;
import com.example.judith.bluetoothtest.R;

/**
 * <b> First activity to welcome the user.</b>
 * <p>
 *  *     This activity first initiates the DataKeeper singleton that is going to store some useful
 *        data. Then its stores in the singleton the result of the user choice about in which
 *  *     mode to start the app, climbing or recording.
 *  * </p>
 */

public class FirstActivity extends AppCompatActivity implements View.OnClickListener {

    private Button mButtonClimb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);

        mButtonClimb = (Button)findViewById(R.id.button_climb_first_activity);
        mButtonClimb.setOnClickListener(this);

        DataKeeper.getInstance();
    }

    @Override
    public void onClick(View v) {
        AccessibilityManager accessibilityManager = (AccessibilityManager)getSystemService(ACCESSIBILITY_SERVICE);
        boolean isAccessibilityEnabled = accessibilityManager.isEnabled();
        boolean isExploredByTouchEnabled = accessibilityManager.isTouchExplorationEnabled();
        if(isAccessibilityEnabled && isExploredByTouchEnabled) {
            DataKeeper.getInstance().setClimbingMode(true);
            Intent talkbackUseIntent = new Intent(FirstActivity.this, FileChoiceActivity.class);
            startActivity(talkbackUseIntent);
        }
        else{
            Intent climbIntent = new Intent(FirstActivity.this, SeparationActivity.class);
            startActivity(climbIntent);
        }
    }
}
