package com.example.judith.bluetoothtest.Classes.UHF_SLS;


/**
 * ByteArrayMessage is an interface for a data structure used to store a byteArray which is in most cases
 * going to be sent to the reader by writing on the characteristics of its Gatt Profile. This
 * structure is used to transfer diverse messages in diverse types (long, short, etc..) in a byte
 * array format.
 */
public interface ByteArrayMessage {

    int getSizeInBytes();

    byte[] getByteArray();
}
