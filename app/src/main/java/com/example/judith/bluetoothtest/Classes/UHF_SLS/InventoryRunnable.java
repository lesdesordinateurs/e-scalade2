//
//
// Smart Label Solutions, Inc.
// InventoryRunnable.java
//
//

package com.example.judith.bluetoothtest.Classes.UHF_SLS;

import android.content.Context;
import android.util.Log;

import com.example.judith.bluetoothtest.Classes.ReaderToActivity;

import java.util.List;

/**
 * InventoryRunnable is the Runnable class representing a tag polling, or tag inventory performed by
 * the UHF SmartLink reader.
 */
public class InventoryRunnable implements Runnable {

    private String LOG_TAG = "Inventory_Runnable";
    private boolean mIsInventorying;

    /**
     * An Interface to communicate with the Activity running.
     */
    private ReaderToActivity mInterface;

    /**
     * The UHF reader instance performing the tag inventory.
     */
    private UHFReader mUHFReader;

    InventoryRunnable(Context context, UHFReader uhfReader) {
        mInterface = (ReaderToActivity)context;
        mIsInventorying = false;
        mUHFReader = uhfReader;
    }

    public void run() {
        //Log.i(LOG_TAG, "Inventory Runnable started");
        mUHFReader.singleInventory();
        if (mIsInventorying) mInterface.postRunnableToHandler(this);

    }

    /**
     * Sets the boolean indicating if the reader is in polling mode or not, i.e. is it's
     * inventorying.
     */
    void setIsInventorying(boolean newValue) {
        mIsInventorying = newValue;
    }

}
